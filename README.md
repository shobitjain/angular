

## Inside work

1. Created a new Angular app (use router system and SCSS).
2. Install Material.io
3. Created a new component with a table(The table contain 6 columns with different types: id, string, number, date)
4. Created service to host the dummy data.
5. Created interface with the data type.
6. Loaded the initial data from the service in the table component.
7. Created a new component, reuse the update component.
8. Used reactive forms.
9. Created a new component that allows the user to insert new Data.
10. Create new component with a card details
11. On row click redirect to a different view showing the full information of the item selected
12. Included picture
13. Added button to EDIT and DELETE
14. on Edit clicked: Update the data
15. on DELETE clicked: Delete the data from the collection