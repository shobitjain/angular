import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataexchangeService, People } from '../_services/dataexchange.service';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {
  idpass:number;
  peopleList: People[];
  constructor(private _activateRoute: ActivatedRoute,
    public _dataexchange:DataexchangeService ) { }

  ngOnInit(): void {
    this.idpass = this._activateRoute.snapshot.params.id;
    this.peopleList = [this._dataexchange.display(this.idpass)];
  }

}