import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataexchangeService, People } from '../_services/dataexchange.service';
import { DatatableComponent } from '../datatable/datatable.component'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

  constructor(private _dataexchange: DataexchangeService, private router: Router,
    private _activateRoute: ActivatedRoute) { }

  employeeForm: FormGroup;
  idpass: number;

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      age: new FormControl('', [Validators.required]),
      pic: new FormControl('', [Validators.required]),
      dob: new FormControl('', [Validators.required])
    });

    this.idpass = this._activateRoute.snapshot.params.id;
    if (this.idpass) {
      this.employeeForm.setValue(this._dataexchange.display(this.idpass))
    }

    console.log(this.employeeForm)
  }
  onSubmit(): void {
    console.log(this.employeeForm)
    if (this.idpass) {
      this._dataexchange.editPerson(this.employeeForm.value)
    } else {
      this._dataexchange.addPerson(this.employeeForm.value)
    }
    this.router.navigate(['/'])
  }

  deletePerson(){
    this._dataexchange.deletePerson(this.idpass)
    this.router.navigate(['/'])
  }
}