import {BrowserModule } from '@angular/platform-browser';
import {NgModule } from '@angular/core';
import {AppComponent } from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CreateComponent } from './create/create.component';
import {DatatableComponent } from './datatable/datatable.component';
import {AppRoutingModule } from './app-routing.module';
import {MatCardModule} from '@angular/material/card';
import {MaterialComponent } from './material/material.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    DatatableComponent,
    MaterialComponent
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
