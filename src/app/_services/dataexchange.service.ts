import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface People {
	id: number,
	name: string,
	email :string,
	age: number,
	dob: Date,
	pic: string
  }

@Injectable({
	providedIn: 'root'
})
export class DataexchangeService {
	constructor() { }
	private peopleList: People[] = [
		{ id: 1, name: 'shobit', email: 'Agniezka@Hollan.ee', age: 22, dob: new Date('1-03-1992'),pic:'http://www.chinadaily.com.cn/entertainment/2007-03/08/xin_350304081150219134911.jpg' },
		{ id: 2, name: 'jain', email: 'Jim@Sheridan.ee', age: 25, dob: new Date('04-13-1996'),pic:'https://pyxis.nymag.com/v1/imgs/05a/e82/d348cb4f4a7ea778119ccc7c6e4541ce78-18-jim1.rvertical.w1200.jpg' },
		{ id: 3, name: 'Gump', email: 'Robert@Zemeckis.ee', age: 20, dob: new Date('07-28-2000'), pic:'https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Jeff_Daniels_May_2018_%28cropped%29.jpg/836px-Jeff_Daniels_May_2018_%28cropped%29.jpg' }
	]

	onPeopleListChanged: BehaviorSubject<People[]> = new BehaviorSubject<People[]>(this.peopleList) 
	

	// getPeople() {
	// 	return this.peopleList;
	// }

	addPerson(newPerson: People) {
		let latestID = 0
		this.peopleList.map(person => {
			if(person.id > latestID){
				latestID = person.id
			}
		})
		newPerson.id = latestID + 1
		this.peopleList.push(newPerson)
		this.onPeopleListChanged.next(this.peopleList.slice())
	
	}
	editPerson(personToEdit: People) {
		const indexToEdit = this.peopleList.findIndex(p => p.id == personToEdit.id)
		this.peopleList[indexToEdit] = {...personToEdit}
		this.onPeopleListChanged.next(this.peopleList.slice())
	}

	deletePerson(idPerson: number){
		const indexToEdit = this.peopleList.findIndex(p => p.id == idPerson)
		this.peopleList.splice(indexToEdit,1)
		console.log(this.peopleList)
		this.onPeopleListChanged.next(this.peopleList.slice())
	}

	display(id:number) {
		return this.peopleList.find(person =>person.id ==id)
	}
}
