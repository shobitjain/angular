import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Routes, RouterModule, Router } from '@angular/router';
import { DataexchangeService, People } from '../_services/dataexchange.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
})
export class DatatableComponent implements OnInit, OnDestroy {
  text:string;
  selectedUser:string;
  peopleList: People[];
  subscription: Subscription;
  constructor(private _dataexchange:DataexchangeService, 
              private _activateRoute: ActivatedRoute,
              private _router: Router,
              ) {
   }

  ngOnInit() {
    this.subscription = this._dataexchange.onPeopleListChanged.subscribe((pl: People[]) => {
      console.log(pl)
      console.log("Hey There is a change!")
      this.peopleList = pl
     
    })
    // this.text= this._dataexchange.display();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }
}
