import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { DatatableComponent } from './datatable/datatable.component';
import { MaterialComponent } from './material/material.component';

const routes: Routes = [
  { path: 'create', component: CreateComponent },
  { path: 'card/:id', component: MaterialComponent },
  { path: 'edit/:id', component: CreateComponent },
  { path: '', component: DatatableComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }